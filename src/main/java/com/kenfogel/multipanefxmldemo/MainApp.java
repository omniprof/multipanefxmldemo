package com.kenfogel.multipanefxmldemo;

import com.kenfogel.multipanefxmldemo.controllers.MasterLayoutController;
import java.io.IOException;
import java.util.ResourceBundle;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Demonstration of a multiple pane application
 *
 * @author Ken Fogel
 */
public class MainApp extends Application {

    // Real programmers use logging, not System.out.println
    private final static Logger LOG = LoggerFactory.getLogger(MainApp.class);

    private BorderPane catPane;
    private BorderPane dogPane;
    private BorderPane fishPane;
    private BorderPane slothPane;

    private Stage primaryStage;
    private BorderPane masterLayout;
    private MasterLayoutController masterController;

    public static void main(String[] args) {
        launch(args);
    }

    /**
     * Create the stage
     *
     * @param primaryStage
     * @throws Exception
     */
    @Override
    public void start(Stage primaryStage) throws Exception {
        this.primaryStage = primaryStage;

        initRootLayout();

        Scene scene = new Scene(masterLayout);
        primaryStage.setScene(scene);
        primaryStage.show();
        LOG.info("Program started");
    }

    /**
     * Set up the master layout
     */
    public void initRootLayout() {

        try {
            // Load root layout from fxml file.
            FXMLLoader loader = new FXMLLoader();
            loader.setResources(ResourceBundle.getBundle("MessagesBundle"));

            loader.setLocation(MainApp.class
                    .getResource("/fxml/MasterLayout.fxml"));
            masterLayout = (BorderPane) loader.load();
            masterController = loader.getController();
            // Send a reference to the masterController so it can change the 
            // center of the borderpane
            masterController.setMasterPane(masterLayout);
            masterController.showSlothFirst();

        } catch (IOException ex) {
            LOG.error(null, ex);
            errorAlert("initRootLayout");
            Platform.exit();
        }
    }

    /**
     * Error message popup dialog
     *
     * @param msg
     */
    private void errorAlert(String msg) {
        Alert dialog = new Alert(Alert.AlertType.ERROR);
        dialog.setTitle(ResourceBundle.getBundle("MessagesBundle").getString("error"));
        dialog.setHeaderText(ResourceBundle.getBundle("MessagesBundle").getString("problem1"));
        dialog.setContentText(ResourceBundle.getBundle("MessagesBundle").getString(msg));
        dialog.show();
    }
}
