package com.kenfogel.multipanefxmldemo.controllers;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.scene.image.ImageView;

/**
 * The controller for the dog. It is not doing anything in this demo
 *
 * @author Ken Fogel
 */
public class DogLayoutController {

    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="dogPic"
    private ImageView dogPic; // Value injected by FXMLLoader

    @FXML // This method is called by the FXMLLoader when initialization is complete
    void initialize() {
        assert dogPic != null : "fx:id=\"dogPic\" was not injected: check your FXML file 'DogLayout.fxml'.";

    }
}
