package com.kenfogel.multipanefxmldemo.controllers;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Alert;
import javafx.scene.layout.BorderPane;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The master controller
 *
 * @author Ken Fogel
 */
public class MasterLayoutController {

    // Real programmers use logging, not System.out.println
    private final static Logger LOG = LoggerFactory.getLogger(MasterLayoutController.class);

    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    private BorderPane catPane;
    private CatLayoutController catController;
    private BorderPane dogPane;
    private DogLayoutController dogController;
    private BorderPane fishPane;
    private FishLayoutController fishController;
    private BorderPane slothPane;
    private SlothLayoutController slothController;
    private BorderPane masterLayout;

    /**
     * Exit the program
     *
     * @param event
     */
    @FXML
    void closeProgram(ActionEvent event) {
        Platform.exit();
    }

    /**
     * Display the about alert (not implemented)
     *
     * @param event
     */
    @FXML
    void showAbout(ActionEvent event) {
        LOG.info("showAbout");
    }

    /**
     * Show the cat pane
     *
     * @param event
     */
    @FXML
    void showCat(ActionEvent event) {
        LOG.info("showcat");
        masterLayout.setCenter(catPane);
    }

    /**
     * Show the dog pane
     *
     * @param event
     */
    @FXML
    void showDog(ActionEvent event) {
        LOG.info("showDog");
        masterLayout.setCenter(dogPane);
    }

    /**
     * Show the fish pane
     *
     * @param event
     */
    @FXML
    void showFish(ActionEvent event) {
        LOG.info("showFish");
        masterLayout.setCenter(fishPane);
    }

    /**
     * Show the sloth pane
     *
     * @param event
     */
    @FXML
    void showSloth(ActionEvent event) {
        LOG.info("showSloth");
        masterLayout.setCenter(slothPane);
    }

    public void showSlothFirst() {
        LOG.info("showSlothFirst");
        masterLayout.setCenter(slothPane);
    }

    /**
     * Create all the panes
     */
    @FXML // This method is called by the FXMLLoader when initialization is complete
    void initialize() {
        createDogPane();
        createCatPane();
        createFishPane();
        createSlothPane();
    }

    /**
     * Create the dog pane
     */
    private void createDogPane() {
        try {
            // Load dog layout from fxml file.
            FXMLLoader loader = new FXMLLoader();
            //loader.setResources(ResourceBundle.getBundle("MessagesBundle"));

            loader.setLocation(MasterLayoutController.class
                    .getResource("/fxml/DogLayout.fxml"));
            // Load the pane into memory, need only be done once
            dogPane = (BorderPane) loader.load();
            // Reference to the pane's controller. Not used in this demo
            dogController = loader.getController();

        } catch (IOException ex) {
            LOG.error(null, ex);
            errorAlert("createDogPane");
            Platform.exit();
        }
    }

    /**
     * Create the cat pane
     */
    private void createCatPane() {
        try {
            // Load cat layout from fxml file.
            FXMLLoader loader = new FXMLLoader();
            //loader.setResources(ResourceBundle.getBundle("MessagesBundle"));

            loader.setLocation(MasterLayoutController.class
                    .getResource("/fxml/CatLayout.fxml"));
            // Load the pane into memory, need only be done once
            catPane = (BorderPane) loader.load();
            // Reference to the pane's controller. Not used in this demo
            catController = loader.getController();

        } catch (IOException ex) {
            LOG.error(null, ex);
            errorAlert("createCatPane");
            Platform.exit();
        }
    }

    /**
     * Create the fish pane
     */
    private void createFishPane() {
        try {
            // Load fish layout from fxml file.
            FXMLLoader loader = new FXMLLoader();
            //loader.setResources(ResourceBundle.getBundle("MessagesBundle"));

            loader.setLocation(MasterLayoutController.class
                    .getResource("/fxml/FishLayout.fxml"));
            // Load the pane into memory, need only be done once
            fishPane = (BorderPane) loader.load();
            // Reference to the pane's controller. Not used in this demo
            fishController = loader.getController();

        } catch (IOException ex) {
            LOG.error(null, ex);
            errorAlert("createFishPane");
            Platform.exit();
        }
    }

    /**
     * Create the sloth pane
     */
    private void createSlothPane() {

        try {
            // Load sloth layout from fxml file.
            FXMLLoader loader = new FXMLLoader();
            //loader.setResources(ResourceBundle.getBundle("MessagesBundle"));

            loader.setLocation(MasterLayoutController.class
                    .getResource("/fxml/SlothLayout.fxml"));
            // Load the pane into memory, need only be done once
            slothPane = (BorderPane) loader.load();
            // Reference to the pane's controller. Not used in this demo
            slothController = loader.getController();

        } catch (IOException ex) {
            LOG.error(null, ex);
            errorAlert("createSlothPane");
            Platform.exit();
        }
    }

    /**
     * Receive a reference to the master layout pane so that events can change
     * the center of the pane
     *
     * @param masterLayout
     */
    public void setMasterPane(BorderPane masterLayout) {
        this.masterLayout = masterLayout;
    }

    /**
     * Error message popup dialog
     *
     * @param msg
     */
    private void errorAlert(String msg) {
        Alert dialog = new Alert(Alert.AlertType.ERROR);
        dialog.setTitle(resources.getString("error"));
        dialog.setHeaderText(resources.getString("problem2"));
        dialog.setContentText(resources.getString(msg));
        dialog.show();
    }
}
